// express server to handle api requests and respong back with a json object, it will use body parser and cors to parse the request and allow cross origin requests

const express = require("express");
const bodyParser = require("body-parser");

const cors = require("cors");
const app = express();
const port = 3300;

app.use(bodyParser.json());
app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.post("/", (req, res) => {
  console.warn(req.body);
  const reqB = req.body;
  res.json({ message: `Hello World! ${reqB.message}` });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
